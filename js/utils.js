var _ = {};

_.noop = function() {};

_.type = function(obj) {
  return {}.toString.call(obj);
};
