// var Application = Application || (function() {
  document.body.addEventListener('touchstart', function(e) {
    e.preventDefault();
  });


  var SolarSystem = new CreateSystem({
    SaturnOrbit: new Orbit('saturn-orbit', {
      parent: document.getElementById('parent'),
    }),
    EarthOrbit: new Orbit('earth-orbit', {
      size: 0.1,
      parent: document.getElementById('parent'),
      velocity: 4
    }),
    MarsOrbit: new Orbit('mars-orbit', {
      size: 0.175,
      parent: document.getElementById('parent'),
      velocity: 2
    })
  });


  var Planets = new CreateSystem({
    Earth: new Planet('planet-earth', {
      parent: document.getElementById('earth-orbit'),
      styles: "border-radius: 100%; background-color: white; background-position: top center; background-size: contain;",
      size: 0.03
    }),
    Saturn: new Planet('planet-saturn', {
      parent: document.getElementById('saturn-orbit'),
      styles: "border-radius: 100%; background-image: url(js.jpg); background-position: top center; background-size: contain;",
      size: 0.1
    }),
    Mars: new Planet('planet-mars', {
      parent: document.getElementById('mars-orbit'),
      styles: "border-radius: 100%; background-image: url(reactjs.png); background-color: #333333; background-position: center center; background-size: cover;",
      size: 0.05
    })
  });

  var MainHeader = new Header('#header');

  var PlayGround = new Playground('pingpong', {
    parent: document.getElementById('main'),
    height: document.getElementById('main'),
    width: document.getElementById('main')
  });

  var PongGame = new PingPong(PlayGround);



// })();
