var PingPong = function PingPong(context) {
  var self = this;

  this.context = context;
  this.canvas = this.context.canvas;
  this.width = context.width;
  this.height = context.height;
  this.colour = context.colour;

  this.assignCoords();
  this.dispatchUI();

};

PingPong.prototype.assignCoords = function() {
  var self = this;
  this.paddleHeight = self.height / 3;
  this.paddleWidth = self.width / 40;

  this.ballInitPos = {
    height: self.width / 40,
    width: self.width / 40,
    x: self.width / 2,
    y: self.height / 2
  };

  this.paddlePos = {
    left: {
      height: self.height / 3,
      width: self.width / 40,
      x: self.width * 0.1,
      y: (self.height / 2) - this.paddleHeight / 2
    },
    right: {
      height: self.height / 3,
      width: self.width / 40,
      x: (self.width - (self.width * 0.1)),
      y: (self.height / 2) - this.paddleHeight / 2

    }
  };

};

PingPong.prototype.dispatchUI = function() {
  var self = this;

  var Paddle = function Paddle(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.x_speed = 0;
    this.y_speed = 0;
  };

  Paddle.prototype.update = function(position) {
    this.x = position.x;
    this.y = position.y;
    this.width = position.width;
    this.height = position.height;
    this.x_speed = 0;
    this.y_speed = 0;
  };

  Paddle.prototype.render = function() {
    self.canvas.fillStyle = "#383838";
    self.canvas.fillRect(this.x, this.y, this.width, this.height);
  };

  var PlayerAI = function PlayerAI(position) {
    Paddle.call(this, position.x, position.y, position.width, position.height);
  };

  PlayerAI.prototype = Object.create(Paddle.prototype);

  var Ball = function Ball(x, y, radius) {
    this.X = x; // X base position constant
    this.Y = y; // Y base position constant

    this.x = x;
    this.y = y;
    this.x_speed = 3;
    this.y_speed = Math.floor(Math.random()*2) ?  Math.ceil(Math.random()*10) : -(Math.ceil(Math.random()*10)) ;

    this.radius = radius || 12;
  };

  Ball.prototype.render = function() {
    self.canvas.beginPath();
    self.canvas.fillRect(0, 0, this.width, this.height);
    self.canvas.arc(this.x, this.y, this.radius, 2 * Math.PI, false);
    self.canvas.fillStyle = "#383838";
    self.canvas.fill();
  };

  Ball.prototype.update = function(op1, op2, radius) {
    this.x += this.x_speed;
    this.y += this.y_speed;

    var top_x = this.x - this.radius/4;
    var top_y = this.y - this.radius/4;

    var bottom_x = this.x + this.radius/4;
    var bottom_y = this.y + this.radius/4;

    if (this.y - this.radius/4 < 0) { // Top wall collision
      this.y = 5;
      this.y_speed = -this.y_speed;
    } else if (this.y + this.radius/2 > self.height) { // Bottom wall collision
      this.y = self.height - 5;
      this.y_speed = -this.y_speed;
    }

    if (this.x < 0 || this.x > self.width) {
      this.x_speed = 3;
      this.y_speed = Math.floor(Math.random()*2) ?  Math.ceil(Math.random()*10) : -(Math.ceil(Math.random()*10));
      this.x = this.X;
      this.y = this.Y;
    }

    if (top_x > this.X) { // going right
      if (top_x < (op2.x + op2.width) && /* |-> */ bottom_x > op2.x /* <-| */ && top_y < (op2.y + op2.height) && bottom_y > op2.y) {
        this.x_speed = -3;
        this.y_speed += (op2.y_speed / 2);
        this.y += this.y_speed;
      }
    } else if (top_x < this.X) { // going left
      if ( /* |-> */ top_x < (op1.x + op1.width) /* <-| */ && bottom_x > op1.x && top_y < (op1.y + op1.height) && bottom_y > op1.y) {
        this.x_speed = 3;
        this.y_speed += (op1.y_speed / 2);
        this.y += this.y_speed;
      }
    }
  };

  Ball.prototype.updateRadius = function(radius) {
    this.radius = radius;
  };

  this.PlayerOne = new PlayerAI(this.paddlePos.left);
  this.PlayerTwo = new PlayerAI(this.paddlePos.right);
  this.GameBall = new Ball(this.ballInitPos.x, this.ballInitPos.y, this.paddlePos.left.width/2);
  this.animate();
};

PingPong.prototype.animate = function() {
  var self = this;

  (function stepFrame() {
    self.update();
    self.render();
    self.updateCoords();
    self.assignCoords();
    window.requestAnimationFrame(stepFrame);
  })();
};

PingPong.prototype.updateCoords = function() {
  this.context.updateCoords();
  this.PlayerOne.update(this.paddlePos.left);
  this.PlayerTwo.update(this.paddlePos.right);
  this.GameBall.updateRadius(this.paddlePos.right.width/2);
  this.height = this.context.height;
  this.width = this.context.width;
};
PingPong.prototype.render = function() {
  this.canvas.fillStyle = this.colour;
  this.canvas.fillRect(0, 0, this.width, this.height);
  this.PlayerOne.render();
  this.PlayerTwo.render();
  this.GameBall.render();
};

PingPong.prototype.update = function() {
  this.GameBall.update(this.PlayerOne, this.PlayerTwo);
};
