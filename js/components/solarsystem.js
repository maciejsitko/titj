
var SolarSystem;

var Circle = function Circle(elementId, config) {

  this.element = document.createElement('div');
  this.element.id = elementId;

  this.stylise(config.styles);
  this.parent = config.parent;

  this.size = config.size;
  this.width = document.body.clientWidth * this.size;

};

Circle.prototype.rotate = function() {
  var self = this;
  this.val = this.val || 0;
  self.element.style.transform = rotator(0);

  function rotator(num) {
    return 'rotate(' + num + 'deg)';
  }

  (function rotateFrame() {
    self.val = self.val % 360;
    self.val += self.velocity;
    self.element.style.transform = rotator(self.val);
    self.rotatingCircle = requestAnimationFrame(rotateFrame);
  })();

};

Circle.prototype.create = function(fn) {
  fn = fn || _.noop;
  var self = this;
  if(window.innerWidth < 900) {
    this.width = (document.body.clientWidth *2) * this.size;
  } else {
    this.width = document.body.clientWidth * this.size;
  }
  this.element.style.width = this.width + 'px';
  this.element.style.position = 'absolute';
  this.element.style.height = this.width + 'px';
  this.appendTo(this.parent);
  fn(self);
  window.addEventListener('resize', function() {
    if(window.innerWidth < 900) {
      self.width = (document.body.clientWidth *2) * self.size;
    } else {
      self.width = document.body.clientWidth * self.size;
    }
    self.element.style.width = self.width + 'px';
    self.element.style.height = self.width + 'px';
    fn(self);
  });
};

Circle.prototype.appendTo = function(parent) {
  var self = this;
  var id = self.element.id;
  parent.appendChild(self.element);
  self.element = document.getElementById(id);

};

Circle.prototype.stylise = function(styles) {
  var self = this;
  var stylesTag = document.createElement('style');
  stylesTag.innerHTML = ['#', self.element.id].join("") + '{ ' + styles + ' }';
  var head = document.getElementsByTagName('head')[0];
  head.appendChild(stylesTag);
};

var Orbit = function Orbit(elementId, config) {
  var defaults = {
    size: 0.35,
    styles: "width: 100px; height: 100px; border-radius: 100%;",
    velocity: 1
  };

  this.velocity = config.velocity || defaults.velocity;

  config.size = config.size || defaults.size;
  config.styles = config.styles || defaults.styles;

  Circle.call(this, elementId, config);

};

Orbit.prototype = Object.create(Circle.prototype);

Orbit.prototype.create = function() {

  Circle.prototype.create.call(this, function(self) {
    var heightPx = Number(self.element.style.height.replace('px', ''));
    var heightPerc = ((heightPx / window.innerHeight) * 100.0);
    var topPos = (100 - heightPerc) / 2;
    self.element.style.border = window.innerWidth * 0.002 + 'px dotted  white';
    self.element.style.top = topPos + "%";
  });
  var self = this;
  var name = this.element.id.slice(0, this.element.id.indexOf('-'));
  this.clicked = false;
  this.element.addEventListener('click', function() {
    var planet = self.element.children[0];
    if (self.clicked === false) {
      planet.style.transition = 'all 1s linear';
      planet.style.transform = 'scale(20,20)';
      self.element.style.zIndex = 500;
      console.log(self.element);
      window.cancelAnimationFrame(self.rotatingCircle);
      self.clicked = true;
    } else {
      self.clicked = false;
      planet.style.transition = 'all 1s linear';
      planet.style.transform = 'scale(1,1)';
      setTimeout(function() {
        self.element.style.zIndex = 'initial';
      },1000);
      self.rotate();
    }
  });
  this.element.style.left = 0;
  this.element.style.right = 0;
  this.element.style.margin = '0 auto';
  this.element.style.borderRadius = '100%';
  this.rotate(this.velocity);

};



var Planet = function Planet(elementId, config) {
  var defaults = {
    size: 0.05,
    styles: "border-radius: 100%; background-image: url(js.jpg); background-position: top center; background-size: contain;",
    velocity: 1
  };
  this.velocity = config.velocity || defaults.velocity;
  config.size = config.size || defaults.size;
  config.styles = config.styles || defaults.styles;
  Circle.call(this, elementId, config);
};

Planet.prototype = Object.create(Circle.prototype);

var CreateSystem = function Create(system) {
  this.system = system;
  this.initialise(this.system);
  //Factory constructor
};

CreateSystem.prototype.initialise = function(system) {
  for (var orbit in system) {
    system[orbit].create();
  }
};
