

var Canvas = function Canvas(elementId, settings) {
  var defaults = {
    parent: document.body,
    height: 300,
    width: 300
  };
  this.parent = settings.parent || defaults.parent;
  this.height = settings.height || defaults.height;
  this.width = settings.width || defaults.width;

  this.canvas = document.createElement('canvas');
  this.canvas.height = this.height;
  this.canvas.width = this.width;

  this.canvas.id = elementId;

  this.appendTo(this.parent);

};

Canvas.prototype.appendTo = function(parent) {
  var id = this.canvas.id;
  parent.appendChild(this.canvas);
  this.canvas = document.getElementById(id);
  this.node = document.getElementById(id);
  if(this.canvas.getContext)  {
    this.canvas = this.canvas.getContext('2d');
    this.canvas.beginPath();
  }
};


Canvas.prototype.fillStyle = function(colour) {
  this.canvas.fillStyle = colour;
};


Canvas.prototype.fillRect = function(x,y, width, height) {
  x = x || 0;
  y = y || 0;
  width = width || this.width;
  height = height || this.height;

  this.canvas.fillRect(x,y,width, height);
};
