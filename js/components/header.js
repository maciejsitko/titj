
var Header = function Header(selector, css) {
  // css.height = css.height || 'auto';
  // css.width = css.width || 'auto';
  //
  this.element = document.querySelector(selector);
  // this.element.style.width = css.width;
  // this.element.style.height = css.height;
  this.currentScroll = 0;
  this.clientHeight = document.getElementById('parent').clientHeight;
  this.scrollY();
};

Header.prototype.scrollY = function() {
  var self = this;
  this.anchorPos = document.getElementById('main').getBoundingClientRect().top;
  window.addEventListener('resize', function() {
    self.anchorPos = document.getElementById('main').getBoundingClientRect().top;
  });

  window.addEventListener('scroll', function(e) {

    var parent = document.getElementById('parent');

    if(window.scrollY > self.currentScroll && window.scrollY < window.innerHeight) {
      parent.style.top = window.scrollY*0.95 + 'px';
      self.currentScroll = window.scrollY;


    } else if(window.scrollY < self.currentScroll && window.scrollY < window.innerHeight){
      parent.style.top = window.scrollY*0.95 + 'px';
      self.currentScroll = window.scrollY;

    }

    console.log(window.scrollY,window.innerHeight);
    if (window.scrollY + (self.element.clientHeight/2) > self.anchorPos) {
      self.element.style.height = 50 + 'px';
      self.element.style.color = 'transparent';
      self.element.querySelector('small').style.top = 15 + 'px';
      self.element.querySelector('small').style.left = '-25%';
    } else {
      self.element.style.height = 100 + 'px';
      self.element.style.color = 'initial';
      self.element.querySelector('small').style.top = 55 + 'px';
      self.element.querySelector('small').style.left = 0;
    }
  });
};
