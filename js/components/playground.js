var Playground = function Playground(elementId, settings) {
  var defaults = {
    colour: '#333333',
    width: 1000,
    height: 700
  };
  this.colour = settings.colour || defaults.colour;
  this.el =  settings.width;
  settings.width = settings.width.clientWidth || defaults.width;
  settings.height = settings.height.clientHeight || defaults.height;

  Canvas.call(this, elementId, settings);
  this.defaultStyles();
  this.render();
  this.updateCoords();
};

Playground.prototype = Object.create(Canvas.prototype);

Playground.prototype.defaultStyles = function() {
  var percentageOf = (this.node.clientHeight / document.getElementById(this.node.parentNode.id).clientHeight) * 100;
  var topPos = (100 - percentageOf) / 2 + '%';
  this.node.style.top = topPos;

  this.node.style.position = 'absolute';
  this.node.style.left = 0;
  this.node.style.right = 0;
  this.node.style.margin = '0 auto';

};

Playground.prototype.render = function() {
  this.canvas.fillStyle = this.colour;
  this.canvas.fillRect(0,0,this.width,this.height);
};

Playground.prototype.updateCoords = function() {
  var self = this;
    self.width = self.el.clientWidth;
    self.height = self.el.clientHeight;
};
